#include <iostream>

#include "bitmap_image.hpp"

class Point{
	public:
		float x;
		float y;
		float red;
		float green;
		float blue;
		
	Point(
		float inX,
		float inY)
		: x(inX), y(inY), red(0), green(0), blue(0) {}

	Point() : x(0), y(0), red(0), green(0), blue(0) {}
	
	~Point(){}
};

class BaryCoord{
	public:
		float lambda1;
		float lambda2;
		float lambda3;
		
	BaryCoord() : lambda1(0), lambda2(0), lambda3(0) {}
	
	public:
		bool isWithinTriangle(){
			return lambda1 > 0.0f
				&& lambda2 > 0.0f
				&& lambda3 > 0.0f;
		}
	
	~BaryCoord(){}
};

std::ostream& operator<<(std::ostream& stream, const Point& point){
	stream << "(" << point.x << "," << point.y << ")";
}

std::ostream& operator<<(std::ostream& stream, const BaryCoord& coord){
	stream << coord.lambda1 << "," << coord.lambda2 << "," << coord.lambda3 << " ";
}

Point findMaxPoint(Point points[3]){
	float maxX = -1;
	float maxY = -1;
	
	for(int i = 0; i < 3; i++){
		if(maxX < points[i].x){
			maxX = points[i].x;
		}
		if(maxY < points[i].y){
			maxY = points[i].y;
		}
	}
	
	return Point(maxX, maxY);
}

Point findMinPoint(Point points[3]){
	float minX = 1000000;
	float minY = 1000000;
	
	for(int i = 0; i < 3; i++){
		if(minX > points[i].x){
			minX = points[i].x;
		}
		if(minY > points[i].y){
			minY = points[i].y;
		}
	}
	
	return Point(minX, minY);
}

BaryCoord findBaryCoord(Point point, Point p1, Point p2, Point p3){
	BaryCoord baryCoord = BaryCoord();
	baryCoord.lambda1 = ((p2.y - p3.y) * (point.x - p3.x) + (p3.x - p2.x) * (point.y - p3.y)) /
		((p2.y - p3.y) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.y - p3.y));
	baryCoord.lambda2 = ((p3.y - p1.y) * (point.x - p3.x) + (p1.x - p3.x) * (point.y - p3.y)) /
		((p2.y - p3.y) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.y - p3.y));
	baryCoord.lambda3 = 1 - baryCoord.lambda1 - baryCoord.lambda2;
	
	return baryCoord;
}

int main(int argc, char** argv) {
    /*
      Part 3:
          You'll need to also request 3 colors from the user each having
          3 floating point values (red, green and blue) that range from 0 to 1.
    */
	
	std::cout << "Please provide three coloured points (ex: x1 y1 r g b)" << std::endl;
	Point points[3];
	for(int i = 0; i < 3; i++){
		Point inPoint = Point();
		std::cin >> inPoint.x >> inPoint.y >> inPoint.red >> inPoint.green >> inPoint.blue;
		points[i] = inPoint;
	}

    bitmap_image rectangleImage(640, 480);
    bitmap_image whiteTriangleImage(640, 480);
    bitmap_image colorImage(640, 480);
	rgb_t white = make_colour(255, 255, 255);
	
	Point maxPoint = findMaxPoint(points);
	Point minPoint = findMinPoint(points);
	for(int i = minPoint.x; i < maxPoint.x; i++){
		for(int j = minPoint.y; j < maxPoint.y; j++){
			rectangleImage.set_pixel(i, j, white); //Part 1
			BaryCoord coord = findBaryCoord(
				Point(i, j),
				points[0],
				points[1],
				points[2]);
			if(coord.isWithinTriangle()){
				whiteTriangleImage.set_pixel(i, j, white); //Part 2
				colorImage.set_pixel(i, j, make_colour( //Part3
					(int) ((coord.lambda1 * points[0].red + coord.lambda2 * points[1].red + coord.lambda3 * points[2].red) * 255),
					(int) ((coord.lambda1 * points[0].green + coord.lambda2 * points[1].green + coord.lambda3 * points[2].green) * 255),
					(int) ((coord.lambda1 * points[0].blue + coord.lambda2 * points[1].blue + coord.lambda3 * points[2].blue) * 255)));
			}
		}
	}
	
    rectangleImage.save_image("../img/rectangle.bmp");
    whiteTriangleImage.save_image("../img/white_triangle.bmp");
    colorImage.save_image("../img/color_triangle.bmp");
    std::cout << "Success" << std::endl;
}
