#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "camera.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window) {
    Matrix trans;

    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
    else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
    else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
    else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
    else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
    else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
    // SCALE
    else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS, 0, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS, 0, 0); }
    else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
    else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }

    return trans * model;
}

bool isSpaceEvent(GLFWwindow *window) {
    static bool pressed = false;

    bool trigger = false;
    if (isPressed(window, GLFW_KEY_SPACE)) {
        pressed = true;
    } else if (pressed && isReleased(window, GLFW_KEY_SPACE)) {
        pressed = false;
        trigger = true;
    }
    return trigger;
}

void processInput(Matrix& model, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    } else if (isSpaceEvent(window)) {
        /**
         * TODO: PART-6 for demo, add code here to change the mode without
         * having massive flickering
         **/
    }
    model = processModel(model, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

GLuint createTexture() {
    GLuint textureID;
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(
		1,
		&textureID);
	glBindTexture(
		GL_TEXTURE_2D,
		textureID);
		
	const int WIDTH = 250;
	const int HEIGHT = 250;
	const int SQUARES_PER_ROW = 10;
	
	GLuint* textureArray = new GLuint[WIDTH * HEIGHT]; // Not sure if this array instantiation is legal?

    /**
     * TODO: Part-2 create the checker texture
     */
	int pixelsPerSideOfSquare = WIDTH / SQUARES_PER_ROW;
	bool currentSquareIsBlack = true;
	int currentIndex = 0;
	for (int i = 0; i < HEIGHT; i++)
	{
		for (int j = 0; j < WIDTH; j++)
		{
			if (currentSquareIsBlack)
			{
				textureArray[currentIndex++] = 0x00000000;
			}
			else
			{
				textureArray[currentIndex++] = 0xFFFFFFFF;
			}
			if (j % pixelsPerSideOfSquare == 0)
			{
				currentSquareIsBlack = !currentSquareIsBlack;
			}
		}
		if (i % pixelsPerSideOfSquare == 0)
		{
			currentSquareIsBlack = !currentSquareIsBlack;
		}
	}
	
	glTexImage2D(
		GL_TEXTURE_2D,
		0,
		GL_RGBA,
		WIDTH,
		HEIGHT,
		0,
		GL_RGBA,
		GL_UNSIGNED_INT_8_8_8_8,
		textureArray);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    return textureID;
}

GLuint loadTexture(const std::string& path, bool flip=true) {
    GLuint textureID;
    glGenTextures(1, &textureID);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(
		GL_TEXTURE_2D,
		textureID);

    int width, height, nrComponents;
    stbi_set_flip_vertically_on_load(flip);
    unsigned char *data = stbi_load(path.c_str(), &width, &height, &nrComponents, 0);
    if (data) {
        GLenum format = 0;
        switch (nrComponents) {
            case 1:
				format = GL_RED;
				break;
            case 3:
				format = GL_RGB;
				break;
            case 4:
				format = GL_RGBA;
				break;
        }

        /**
         * TODO: Part-3 create a texture map for an image
         */
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			format,
			width,
			height,
			0,
			format,
			GL_UNSIGNED_BYTE,
			data);
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    } else {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }

    return textureID;
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, 10);

    Camera camera;
    camera.projection = projection;
    camera.eye = Vector(0, 0, 3);
    camera.origin = Vector(0, 0, 0);
    camera.up = Vector(0, 1, 0);

    // setup the shader
    Shader shader("../vert.glsl", "../frag.glsl");

    /* init the shape */
    Cube cube;
    Sphere sphere(20, 1);

    // copy vertex data
    GLuint cubeVbo;
    glGenBuffers(1, &cubeVbo);
    glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
    glBufferData(GL_ARRAY_BUFFER, cube.size(), cube.data(), GL_STATIC_DRAW);

    // describe vertex layout
    GLuint cubeVao;
    glGenVertexArrays(1, &cubeVao);
    glBindVertexArray(cubeVao);

    // setup position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);
	
    GLuint sphereVbo;
	glGenBuffers(1, &sphereVbo);

    // describe vertex layout
    GLuint sphereVao;
    glGenVertexArrays(1, &sphereVao);
    //glBindVertexArray(sphereVao);

    /** TODO: Part1 add vertex attribute pointer for texture coordinates */

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // init the model matrix
    Matrix model;

    // setup the textures
    /** TODO: Part2 create and bind the texture. */
	//int textureIndex = createTexture();
	
	
	//glActiveTexture(GL_TEXTURE0);
	GLuint textureQuestion = loadTexture("../img/question.png");
	//glBindTexture(GL_TEXTURE_2D, textureQuestion);
	
	//glActiveTexture(GL_TEXTURE1);
	GLuint textureChecker = createTexture();
	//glBindTexture(GL_TEXTURE_2D, textureChecker);
	
	int textureIndex = 0;
		
	// Create a circular linked list of the various texture indices.
	// Set textureIndex to circularLinkedList.next() when the user hits space.
	// Implement multiple models with the same approach (different key to switch between models).
	int shapeUse = 0;

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(model, window);
		
		if(isPressed(window, ' ')){
			shapeUse = abs(shapeUse - 1);
			
			if(shapeUse == 0){
				glBindBuffer(GL_ARRAY_BUFFER, cubeVbo);
				glBufferData(GL_ARRAY_BUFFER, cube.size(), cube.data(), GL_STATIC_DRAW);

				glBindVertexArray(cubeVao);
			} else if(shapeUse == 1){
				glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
				glBufferData(GL_ARRAY_BUFFER, sphere.size(), sphere.data(), GL_STATIC_DRAW);
				
				glBindVertexArray(sphereVao);
			}
			
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(3*sizeof(float)));
			glEnableVertexAttribArray(1);
			
			Sleep(250);
		}
		
		if(isPressed(window, GLFW_KEY_BACKSLASH)){
			textureIndex = abs(textureIndex - 1);
			
			Sleep(250);
		}

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        // render the object
        shader.use();
        Uniform::set(shader.id(), "model", model);
        Uniform::set(shader.id(), "projection", camera.projection);
        Uniform::set(shader.id(), "camera", camera.look_at());
        Uniform::set(shader.id(), "eye", camera.eye);
		Uniform::set(shader.id(), "aSampler", textureIndex);
		
        // render the cube
		if(shapeUse == 0){
			glBindVertexArray(cubeVao);
			glDrawArrays(GL_TRIANGLES, 0, cube.size());
		} else if(shapeUse == 1){
			glBindVertexArray(sphereVao);
			glDrawArrays(GL_TRIANGLES, 0, sphere.size());
		}

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
