#version 330 core

uniform sampler2D aSampler;

in vec2 ourTextureCoordinates;

out vec4 fragColor;

/**
 * TODO: PART-1 update the fragment shader to get the texture coordinates from
 * the vertex shader
 */

/**
 * TODO: PART-3 update the fragment shader to get the fragColor color from the
 * texture, and add the sampler2D.
 */

void main() {
	float xTextureCoordinate =
		ourTextureCoordinates[0];
	float yTextureCoordinate =
		ourTextureCoordinates[1];
    //fragColor = vec4(				// This line is for PART-1
	//	xTextureCoordinate,			// Replace it with commented out code below for actual texture mapping
	//	yTextureCoordinate,
	//	0,
	//	1);
	
	vec4 color = texture(
		aSampler,
		ourTextureCoordinates);
	fragColor = color;
}
