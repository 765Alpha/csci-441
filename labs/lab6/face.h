#ifndef _CSCI441_FACE_H_
#define _CSCI441_FACE_H_

#include <cstdlib>
#include <vector>

class Vertex;
class Face{
	public:
		Vertex* v1;
		Vertex* v2;
		Vertex* v3;
		int v1Index, v2Index, v3Index;
		
		Face(
			Vertex* inVUno,
			Vertex* inVDos,
			Vertex* inVTres)
		{			
			v1 = inVUno;
			v2 = inVDos;
			v3 = inVTres;
		}
		
		Face() {}
};

#endif