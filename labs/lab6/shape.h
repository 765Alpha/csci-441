#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>
#include "face.h"
#include "vertex.h"

template <typename T, typename N, typename C>
void add_vertex(
		T& coords,
		const N& x, const N& y, const N& z,
		const C& r, const C& g, const C& b,
		const Vector& n=Vector(1,0,0),
		const Vector& smoothN=Vector(1,0,0),
		bool with_noise=false) {
	// adding color noise makes it easier to see before shading is implemented
	float noise = 1-with_noise*(rand()%150)/100.;
	coords.push_back(x);
	coords.push_back(y);
	coords.push_back(z);
	coords.push_back(r*noise);
	coords.push_back(g*noise);
	coords.push_back(b*noise);

	Vector normal = n.normalized();
	coords.push_back(normal.x());
	coords.push_back(normal.y());
	coords.push_back(normal.z());
	
	Vector smoothNormal = smoothN.normalized();
	coords.push_back(smoothNormal.x());
	coords.push_back(smoothNormal.y());
	coords.push_back(smoothNormal.z());
}

class Primitive{
	public:
		Vector calculateFastedNormal(GLfloat* threePoints)
		{
			GLfloat x1, x2, x3, y1, y2, y3, z1, z2, z3;
			GLfloat x1Vec, x2Vec, y1Vec, y2Vec, z1Vec, z2Vec;
			GLfloat xNorm, yNorm, zNorm;
			x1 = threePoints[0];
			x2 = threePoints[3];
			x3 = threePoints[6];
			y1 = threePoints[1];
			y2 = threePoints[4];
			y3 = threePoints[7];
			z1 = threePoints[2];
			z2 = threePoints[5];
			z3 = threePoints[8];
			x1Vec = x3 - x1;
			x2Vec = x2 - x1;
			y1Vec = y3 - y1;
			y2Vec = y2 - y1;
			z1Vec = z3 - z1;
			z2Vec = z2 - z1;
			xNorm = y1Vec * z2Vec - z1Vec * y2Vec;
			yNorm = z1Vec * x2Vec - x1Vec * z2Vec;
			zNorm = x1Vec * y2Vec - y1Vec * x2Vec;
		
			Vector normal = Vector(
				xNorm,
				yNorm,
				zNorm);
			
			return normal;
		}
		
		Vector calculateSmoothNormal(Vertex v)
		{
			
		}
};

class DiscoCube : public Primitive{
public:
	std::vector<float> coords;
	DiscoCube() : coords{
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,  0.0f,  0.0f, -1.0,
		 0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,  0.0f,  0.0f, -1.0,
		 0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,  0.0f,  0.0f, -1.0,
		 0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,  0.0f,  0.0f, -1.0,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,  0.0f,  0.0f, -1.0,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  0.0f, -1.0,  0.0f,  0.0f, -1.0,

		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,  0.0f,  0.0f,  1.0,
		 0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,  0.0f,  0.0f,  1.0,
		 0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,  0.0f,  0.0f,  1.0,
		 0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,  0.0f,  0.0f,  1.0,
		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,  0.0f,  0.0f,  1.0,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,  0.0f,  0.0f,  1.0,  0.0f,  0.0f,  1.0,

		-0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0, -1.0f,  0.0f,  0.0,
		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0, -1.0f,  0.0f,  0.0,
		-0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0, -1.0f,  0.0f,  0.0,
		-0.5f, -0.5f, -0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0, -1.0f,  0.0f,  0.0,
		-0.5f, -0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0, -1.0f,  0.0f,  0.0,
		-0.5f,  0.5f,  0.5f,  1.0f, 1.0f, 0.0f, -1.0f,  0.0f,  0.0, -1.0f,  0.0f,  0.0,

		 0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,  1.0f,  0.0f,  0.0,
		 0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,  1.0f,  0.0f,  0.0,
		 0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,  1.0f,  0.0f,  0.0,
		 0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,  1.0f,  0.0f,  0.0,
		 0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,  1.0f,  0.0f,  0.0,
		 0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,  1.0f,  0.0f,  0.0,  1.0f,  0.0f,  0.0,

		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,  0.0f, -1.0f,  0.0,
		 0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,  0.0f, -1.0f,  0.0,
		 0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,  0.0f, -1.0f,  0.0,
		 0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,  0.0f, -1.0f,  0.0,
		-0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,  0.0f, -1.0f,  0.0,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f, -1.0f,  0.0,  0.0f, -1.0f,  0.0,

		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,  0.0f,  1.0f,  0.0,
		 0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,  0.0f,  1.0f,  0.0,
		 0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,  0.0f,  1.0f,  0.0,
		 0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 1.0f,  0.0f,  1.0f,  0.0,  0.0f,  1.0f,  0.0,
		-0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,  0.0f,  1.0f,  0.0,
		-0.5f,  0.5f, -0.5f,  1.0f, 1.0f, 0.0f,  0.0f,  1.0f,  0.0,  0.0f,  1.0f,  0.0
	} {}

};

class Cylinder : public Primitive{
public:
	std::vector<float> coords;
	Cylinder(unsigned int n, float r, float g, float b) {
		double step_size = 2*M_PI / n;
		double c_x=0;
		double c_y=0;
		double h = .5;
		float radius = .5;

		for (int i = 0; i < n; ++i) {
			// vertex i
			double theta_i = i*step_size;
			double vi_x = radius*cos(theta_i);
			double vi_y = radius*sin(theta_i);

			// vertex i+1
			double theta_ip1 = ((i+1)%n)*step_size;
			double vip1_x = radius*cos(theta_ip1);
			double vip1_y = radius*sin(theta_ip1);

			add_vertex(coords, vi_x, -h, vi_y, r, g, b);
			add_vertex(coords, vi_x, h, vi_y, r, g, b);
			add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);

			// add triangle vip1L, viH, vip1H
			add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
			add_vertex(coords, vi_x, h, vi_y, r, g, b);
			add_vertex(coords, vip1_x, h, vip1_y, r, g, b);

			// add high triangle vi, vip1, 0
			Vector nh(0, 1, 0);
			add_vertex(coords, vip1_x, h, vip1_y, r, g, b);
			add_vertex(coords, vi_x, h, vi_y, r, g, b);
			add_vertex(coords, c_x, h, c_y, r, g, b);

			// // add low triangle vi, vip1, 0
			Vector nl(0, -1, 0);
			add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
			add_vertex(coords, c_x, -h, c_y, r, g, b);
			add_vertex(coords, vi_x, -h, vi_y, r, g, b);
		}
	}
};


class Cone : public Primitive{
public:
	std::vector<float> coords;
	Cone(unsigned int n, float r, float g, float b) {

		double step_size = 2*M_PI / n;
		double c_x=0;
		double c_y=0;
		double h = .5;
		float radius = .5;

		for (int i = 0; i < n; ++i) {
			// vertex i
			double theta_i = i*step_size;
			double vi_x = radius*cos(theta_i);
			double vi_y = radius*sin(theta_i);

			// vertex i+1
			double theta_ip1 = ((i+1)%n)*step_size;
			double vip1_x = radius*cos(theta_ip1);
			double vip1_y = radius*sin(theta_ip1);

			// add triangle viL, viH, vip1L
			add_vertex(coords, vi_x, -h, vi_y, r, g, b);
			add_vertex(coords, c_x, h, c_y, r, g, b);
			add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);

			// // add low triangle vi, vip1, 0
			add_vertex(coords, vip1_x, -h, vip1_y, r, g, b);
			add_vertex(coords, c_x, -h, c_y, r, g, b);
			add_vertex(coords, vi_x, -h, vi_y, r, g, b);
		}
	}
};

class Sphere : public Primitive{
	double x(float r, float phi, float theta){
		return r*cos(theta)*sin(phi);
	}

	double y(float r, float phi, float theta){
		return r*sin(theta)*sin(phi);
	}

	double z(float r, float phi, float theta){
		return r*cos(phi);
	}

public:
	std::vector<float> coords;
	Sphere(unsigned int n, float radius, float r, float g, float b) {
		int n_steps = (n%2==0) ? n : n+1;
		double step_size = 2*M_PI / n_steps;

		for (int i = 0; i < n_steps/2.0; ++i) {
			for (int j = 0; j < n_steps; ++j) {
				double phi_i = i*step_size;
				double phi_ip1 = ((i+1)%n_steps)*step_size;
				double theta_j = j*step_size;
				double theta_jp1 = ((j+1)%n_steps)*step_size;

				// vertex i,j
				double vij_x = x(radius, phi_i, theta_j);
				double vij_y = y(radius, phi_i, theta_j);
				double vij_z = z(radius, phi_i, theta_j);

				// vertex i+1,j
				double vip1j_x = x(radius, phi_ip1, theta_j);
				double vip1j_y = y(radius, phi_ip1, theta_j);
				double vip1j_z = z(radius, phi_ip1, theta_j);

				// vertex i,j+1
				double vijp1_x = x(radius, phi_i, theta_jp1);
				double vijp1_y = y(radius, phi_i, theta_jp1);
				double vijp1_z = z(radius, phi_i, theta_jp1);

				// vertex i+1,j+1
				double vip1jp1_x = x(radius, phi_ip1, theta_jp1);
				double vip1jp1_y = y(radius, phi_ip1, theta_jp1);
				double vip1jp1_z = z(radius, phi_ip1, theta_jp1);

				// add triangle
				add_vertex(coords, vij_x, vij_y, vij_z, r, g, b);
				add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
				add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);

				// add triange
				add_vertex(coords, vijp1_x, vijp1_y, vijp1_z, r, g, b);
				add_vertex(coords, vip1jp1_x, vip1jp1_y, vip1jp1_z, r, g, b);
				add_vertex(coords, vip1j_x, vip1j_y, vip1j_z, r, g, b);
			}
		}
	}
};

class Torus : public Primitive{
	double x(float c, float a, float phi, float theta) {
		return (c+a*cos(theta))*cos(phi);
	}

	double y(float c, float a, float phi, float theta) {
		return (c+a*cos(theta))*sin(phi);
	}

	double z(float c, float a, float phi, float theta) {
		return a*sin(theta);
	}

public:
	std::vector<float> coords;
	Torus(unsigned int n, float c, float a, float r, float g, float b) {

		float step_size = 2*M_PI / n;
		float c_x=0;
		float c_y=0;
		float h = .5;
		float radius = .5;
		
		Vertex* uniqueVertices = new Vertex[n*n];
		int verticesSize = 0;
		
		for (int i = 0; i < n; i++)
		{
			float theta = i * step_size;
			for (int j = 0; j < n; j++)
			{
				float phi = j * step_size;
				
				Vertex* vertexToBe;
				
				Vector tempCoords = Vector(
					x(c, a, phi, theta),
					y(c, a, phi, theta),
					z(c, a, phi, theta));
				Vector tempColor = Vector(r, g, b);
				
				Vertex temp = Vertex(
					&tempCoords,
					&tempColor);
				
				vertexToBe = &temp;
				uniqueVertices[verticesSize++] = *vertexToBe;
				}
		}
		
		Face* faces = new Face[n*n*2];
		int facesSize = n*n*2;

		for (int i = 0; i < verticesSize; i++)
		{
			Face face1;
			Face face2;

			Vertex* v1 = &uniqueVertices[i];
			Vertex* v2 = &uniqueVertices[(i + n) % (n*n)];
			Vertex* v3 = &uniqueVertices[(i + 1) % (n*n)];
			Vertex* v4 = &uniqueVertices[(i + n + 1) % (n*n)];

			face1 = Face(
				v1,
				v2,
				v3);
			
			faces[i * 2] = face1;
			
			v1->addFace(face1);
			v2->addFace(face1);
			v3->addFace(face1);
			
			//std::cout << *v1 << *v2 << *v3 << std::endl;
			
			face2 = Face(
				v3,
				v2,
				v4);
				
			faces[i * 2 + 1] = face2;
			
			v3->addFace(face2);
			v2->addFace(face2);
			v4->addFace(face2);
		}

		for(int i = 0; i < facesSize; i++) {
			Face currentFace = faces[i];
			
			float normal1Args[9] = {
				currentFace.v1->getX(),
				currentFace.v1->getY(),
				currentFace.v1->getZ(),
				currentFace.v2->getX(),
				currentFace.v2->getY(),
				currentFace.v2->getZ(),
				currentFace.v3->getX(),
				currentFace.v3->getY(),
				currentFace.v3->getZ()};
			Vector v1FascettedNormal = calculateFastedNormal(normal1Args);
			
			Vector v1SmoothNormal = currentFace.v1->calculateSmoothNormal();
			
			add_vertex(
				coords,
				currentFace.v1->getX(),
				currentFace.v1->getY(),
				currentFace.v1->getZ(),
				r,
				g,
				b,
				v1FascettedNormal,
				v1SmoothNormal);
			
			Vector v2SmoothNormal = currentFace.v2->calculateSmoothNormal();
			
			add_vertex(
				coords,
				currentFace.v2->getX(),
				currentFace.v2->getY(),
				currentFace.v2->getZ(),
				r,
				g,
				b,
				v1FascettedNormal,
				v2SmoothNormal);
			
			Vector v3SmoothNormal = currentFace.v3->calculateSmoothNormal();
			
			add_vertex(
				coords,
				currentFace.v3->getX(),
				currentFace.v3->getY(),
				currentFace.v3->getZ(),
				r,
				g,
				b,
				v1FascettedNormal,
				v3SmoothNormal);
		}
	}
};



#endif
