#ifndef _CSCI441_VERTEX_H_
#define _CSCI441_VERTEX_H_

#include <cstdlib>
#include <vector>
#include "face.h"

class Vertex{
	public:
		Vector* coords;
		float xC;
		float yC;
		float zC;
		Vector* color;
		Face* faces = new Face[0];
		int facesSize = 0;
		
		Vertex(
			Vector* inCoords,
			Vector* inColor)
		{
			Vector tempCoords = Vector(
				inCoords->x(),
				inCoords->y(),
				inCoords->z());
		
			coords = &tempCoords;
			xC = inCoords->x();
			yC = inCoords->y();
			zC = inCoords->z();
			
			Vector tempColor = Vector(
				inColor->x(),
				inColor->y(),
				inColor->z());
			
			color = &tempColor;
		}
		
		Vertex() {}
		
		Vector calculateSmoothNormal(){
			float sumNormalX = 0;
			float sumNormalY = 0;
			float sumNormalZ = 0;
				
			for(int i = 0; i < facesSize; i++){
				Vector faceNormal = calculateFaceNormal(&faces[i]);
				sumNormalX += faceNormal.x();
				sumNormalY += faceNormal.y();
				sumNormalZ += faceNormal.z();
			}
			
			return Vector(
				sumNormalX / facesSize,
				sumNormalY / facesSize,
				sumNormalZ / facesSize);
		}
		
		Vector calculateFaceNormal(Face* face){
			GLfloat x1, x2, x3, y1, y2, y3, z1, z2, z3;
			GLfloat x1Vec, x2Vec, y1Vec, y2Vec, z1Vec, z2Vec;
			GLfloat xNorm, yNorm, zNorm;
			x1 = face->v1->getX();
			x2 = face->v2->getX();
			x3 = face->v3->getX();
			y1 = face->v1->getY();
			y2 = face->v2->getY();
			y3 = face->v3->getY();
			z1 = face->v1->getZ();
			z2 = face->v2->getZ();
			z3 = face->v3->getZ();
			x1Vec = x3 - x1;
			x2Vec = x2 - x1;
			y1Vec = y3 - y1;
			y2Vec = y2 - y1;
			z1Vec = z3 - z1;
			z2Vec = z2 - z1;
			xNorm = y1Vec * z2Vec - z1Vec * y2Vec;
			yNorm = z1Vec * x2Vec - x1Vec * z2Vec;
			zNorm = x1Vec * y2Vec - y1Vec * x2Vec;
		
			Vector normal = Vector(
				xNorm,
				yNorm,
				zNorm);
				
			return normal.normalized();
		}
		
		void addFace(Face face){	
			Face* newSetOfFaces = new Face[facesSize + 1];
			
			for(int i = 0; i < facesSize; i++) {
				newSetOfFaces[i] = faces[i];
			}
			
			newSetOfFaces[facesSize] = face;
			facesSize++;
			
			faces = newSetOfFaces;
		}
		
		float getX(){
			//return coords->x();
			return xC;
		}
		
		float getY(){
			//return coords->y();
			return yC;
		}
		
		float getZ(){
			//return coords->z();
			return zC;
		}
		
		float getRed(){
			return color->x();
		}
		
		float getGreen(){
			return color->y();
		}
		
		float getBlue(){
			return color->z();
		}
		
		friend std::ostream& operator<<(std::ostream& os, const Vertex& v) {
			os << "(" << v.xC << ", " << v.yC << ", " << v.zC << ")";
			return os;
		}
};

#endif