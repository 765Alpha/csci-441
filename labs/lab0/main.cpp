#include <iostream>
#include <string>
using namespace std;

class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
    }
	
	//Part 8 - Default constructor
	Vector3() : x(0), y(0), z(0){}

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
    }
};

Vector3 add(Vector3 v, Vector3 v2) {
	Vector3 addedVector = Vector3(
		v.x + v2.x,
		v.y + v2.y,
		v.z + v2.z);
		
	return addedVector;
}

Vector3 operator+(Vector3 v, Vector3 v2){
	Vector3 addedVector = Vector3(
		v.x + v2.x,
		v.y + v2.y,
		v.z + v2.z);
		
	return addedVector;
}

std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
	stream << v.x << ", " << v.y << ", " << v.z;
	return stream;
}

//Part 1
int main(int argc, char** argv){
	//Part 4a
	for(int i = 0; i < argc; i++){
		std::cout << argv[i] << std::endl;
	}
	
	//Part 4b
	std::cout << "Please provide your name: ";
	string name;
	std::cin >> name;
	std::cout << "hello " << name << std::endl;
	
	Vector3 v(1,2,3);
	Vector3 v2(4,5,6);
	
	//Part 5
	Vector3 result = add(v, v2);
	
	std::cout << "Part 5: " << result.x << ", " << result.y << ", " << result.z << std::endl;

	//Part 6
	std::cout << "Part 6: " << v+v2 << std::endl;
	
	//Part 7
	Vector3 stackVector = Vector3();
	stackVector.y = 5;
	std::cout << "Part 7: " << stackVector << std::endl;
	
	//Part 8
	std::cout << "Part 8: " << std::endl;
	Vector3 vectors[10];
	for(int i = 0; i < 10; i++){
		vectors[i] = Vector3();
	}
	for(int i = 0; i < 10; i++){
		vectors[i].y = 5;
		std::cout << i+1 << ": " << vectors[i] << std::endl;
	}
	
	std::cout << "Done." << std::endl;
}