#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <Controller.h>
#include <Matrix.h>
#include <PrimitiveShapes.h>
#include <PrimitiveIO.h>
#include <Windows.h>

#include <csci441/shader.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
int currentWidth = SCREEN_WIDTH;
int currentHeight = SCREEN_HEIGHT;
float windowRatio = float(SCREEN_HEIGHT) / float(SCREEN_WIDTH);
Controller controller;

void updateProjectionAndCameraMatrix(
	Shader shader,
	Controller controller);
void updateModelMatrix(
	Shader shader,
	Controller controller);
Matrix perspectiveMatrix(
	GLfloat l,
	GLfloat r,
	GLfloat b,
	GLfloat t,
	GLfloat n,
	GLfloat f);
Matrix translateMatrix(
	float x,
	float y,
	float z);
Matrix scaleMatrix(
	GLfloat x,
	GLfloat y);
Matrix rotationMatrixZ(float theta);
Matrix orthographicMatrix(
	GLfloat l,
	GLfloat r,
	GLfloat b,
	GLfloat t,
	GLfloat n,
	GLfloat f);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	currentWidth = width;
	currentHeight = height;
	windowRatio = float(height) / float(width);
}
		
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
	if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
		controller.clickHeldDown = true;
		controller.previousRotationZ = controller.zRotationDegree;
		glfwGetCursorPos(window, &controller.startingClickX, &controller.startingClickY);
	} else {
		controller.clickHeldDown = false;
	}
}

void processInput(GLFWwindow *window, Shader &shader) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
}

void errorCallback(int error, const char* description) {
	fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
	GLFWwindow* window;

	glfwSetErrorCallback(errorCallback);

	/* Initialize the library */
	if (!glfwInit()) { return -1; }

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);
	
	// tell glfw what to do on resize
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	// init glad
	if (!gladLoadGL()) {
		std::cerr << "Failed to initialize OpenGL context" << std::endl;
		glfwTerminate();
		return -1;
	}

	/* init the model */
	Primitive primitive = Circle();
	float* primitiveVertices = &primitive.coords[0];
	GLfloat primitiveVerticesSize = primitive.verticesSize;
	
	// copy vertex data
	float* vertices = primitiveVertices;
	float verticesSize = primitive.verticesSize;
	
	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_DYNAMIC_DRAW);

	// describe vertex layout
	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
		(void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
		(void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// create the shaders
	Shader shader("../vert.glsl", "../frag.glsl");

	// setup the textures
	shader.use();
	
	// and use z-buffering
	glEnable(GL_DEPTH_TEST);

	/* Loop until the user closes the window */
	controller.window = window;
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	controller.screenWidth = currentWidth;
	controller.screenHeight = currentHeight;
	while (!glfwWindowShouldClose(window)) {
		// process input
		processInput(window, shader);

		
		/* Render here */
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		// create transformation matrix and pass it to shader
		controller.recieveUserInput();
		
		if(controller.loadPrimitive){
			FileInput fileIn = FileInput();
			float* customVertices = fileIn.startLoad();
			primitive = Custom(customVertices, fileIn.currentVerticeAmount);
			verticesSize = primitive.verticesSize;
			vertices = &primitive.coords[0];
			
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_DYNAMIC_DRAW);
			
			glBindVertexArray(VAO);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)(3 * sizeof(float)));
			glEnableVertexAttribArray(1);
			
			controller = Controller();
			controller.window = window;
			glfwSetMouseButtonCallback(window, mouse_button_callback);
		}
		
		
		updateModelMatrix(shader, controller);
		updateProjectionAndCameraMatrix(shader, controller);

		controller.loadPrimitive = false;
		
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, primitive.numberOfVertices);

		/* Swap front and back and poll for io events */
		glfwSwapBuffers(window);
		glfwPollEvents();

		Sleep(17);
	}

	glfwTerminate();
	return 0;
}


void updateProjectionAndCameraMatrix(
	Shader shader,
	Controller controller)
{
	Matrix viewMatrix = orthographicMatrix(
		-1.0f / windowRatio,
		1.0f / windowRatio,
		-1.0f,
		1.0f,
		-1.0f,
		1.0f);
	Matrix cameraMatrix = translateMatrix(
		controller.cameraLocationX,
		controller.cameraLocationY,
		controller.cameraLocationZ);
	
	viewMatrix.passToShader(
		shader,
		"perspectiveMatrix");
	
	cameraMatrix.passToShader(
		shader,
		"cameraMatrix");
		
	//std::cout << cameraMatrix << std::endl;
}

void updateModelMatrix(
	Shader shader,
	Controller controller)
{
	Matrix translateM = translateMatrix(
		controller.modelTranslateX,
		controller.modelTranslateY,
		0.0f);
	Matrix z = rotationMatrixZ(controller.zRotationDegree);
	Matrix scaleM = scaleMatrix(
		controller.modelScale,
		controller.modelScale);

	Matrix* modelMatrix = &translateM;
	Matrix tempTranslateRotation = *modelMatrix * z;
	modelMatrix = &(tempTranslateRotation);
	Matrix tempTranslateRotationScale = *modelMatrix * scaleM;
	modelMatrix = &(tempTranslateRotationScale);

	modelMatrix->passToShader(
		shader,
		"modelMatrix");
}

Matrix translateMatrix(
	float x,
	float y,
	float z)
{
	GLfloat matrixElements[16] = {
		1.0f, 0.0f, 0.0f, x,
		0.0f, 1.0f, 0.0f, y,
		0.0f, 0.0f, 1.0f, z,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	Matrix matrix(4, 4);
	matrix.populate(matrixElements);

	return matrix;
}

Matrix scaleMatrix(
	GLfloat scaleX,
	GLfloat scaleY)
{
	GLfloat matrixElements[16] = {
		scaleX, 0.0f, 0.0f, 0.0f,
		0.0f, scaleY, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	Matrix matrix(4, 4);
	matrix.populate(matrixElements);

	return matrix;
}

Matrix rotationMatrixZ(float theta)
{
	GLfloat pi = 3.14159265f;
	float c = cos(theta * pi / 180);
	float s = sin(theta * pi / 180);

	GLfloat elements[16] = {
		  c ,  -s , 0.0f, 0.0f,
		  s ,   c , 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
	Matrix matrix(4, 4);
	matrix.populate(elements);

	return matrix;
}

Matrix orthographicMatrix(
	GLfloat l,
	GLfloat r,
	GLfloat b,
	GLfloat t,
	GLfloat n,
	GLfloat f)
{
	Matrix orthographic = Matrix(4, 4);
	GLfloat x1 = 2 / (r - l);
	GLfloat x2 = -((r + l) / (r - l));
	GLfloat y1 = 2 / (t - b);
	GLfloat y2 = -((t + b) / (t - b));
	GLfloat z1 = 2 / (f - n);
	GLfloat z2 = (n + f) / (n - f);

	GLfloat matrixElements[16] = {
		x1 , 0.0f, 0.0f,  x2,
		0.0f,  y1 , 0.0f,  y2,
		0.0f, 0.0f,  z1 ,  z2,
		0.0f, 0.0f, 0.0f, 1.0f
	};
	
	orthographic.populate(matrixElements);
	
	return orthographic;
}

Matrix perspectiveMatrix(
	GLfloat l,
	GLfloat r,
	GLfloat b,
	GLfloat t,
	GLfloat n,
	GLfloat f)
{
	Matrix perspectiveMatrix = Matrix(4, 4);
	GLfloat x1 = (2 * abs(n)) / (r - l);
	GLfloat y1 = (2 * abs(n)) / (t - b);
	GLfloat z1 = (abs(n) + abs(f)) / (abs(n) - abs(f));
	GLfloat x3 = (r + l) / (r - l);
	GLfloat y3 = (t + b) / (t - b);
	GLfloat z4 = (2 * (abs(n) * abs(f))) / (abs(n) - abs(f));

	//GLfloat x1 = 1 / r;
	//GLfloat y1 = 1 / t;
	//GLfloat z1 = -2 / (f - n);
	//GLfloat x3 = 0.0f;
	//GLfloat y3 = 0.0f;
	//GLfloat z4 = -(f + n) / (f - n);

	//GLfloat elements[16] = {
	//	x1, 0.0f, x3, 0.0f,
	//	0.0f, y1, y3, 0.0f,
	//	0.0f, 0.0f, z1, z4,
	//	0.0f, 0.0f, -1.0f, 0.0f
	//};

	GLfloat elements[16] = {
		x1, 0.0f, x3, 0.0f,
		0.0f, y1, y3, 0.0f,
		0.0f, 0.0f, z1, z4,
		0.0f, 0.0f, 0.0f, 1.0f
	};
	
	perspectiveMatrix.populate(elements);
	
	return perspectiveMatrix;
}