Get ready to take off a bunch of points!

You start with a red square then can replace it with a single primitive to the center location.

What's working:
1) You can click anywhere on the window and drag to turn the object that much.
	While doing this, the distance you drag will be the scaling on the current model.
2) You cannot save primitives, but in the userGeneratedPrimitives you can make a text file
	that follows the format like 'test.txt' (which is a triangle with a point at 0,0).
	To load from a file, press ',' and type into the console the name of the file w/out the extension.
3) When resizing the window, the object will key off the y-axis, meaning it will keep its form regardless
	of the window ratio.
4) The camera is supposed to move with the arrow keys, but for some issue it only glitches.
5) Keyboard commands:
	rotate: 'o' and 'p'
6) Cannot switch between or to pre-built primitives.