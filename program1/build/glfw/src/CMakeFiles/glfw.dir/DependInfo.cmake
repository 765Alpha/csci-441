# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "M:/Classes/CSCI_441/lib/glfw/src/context.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/context.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/egl_context.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/egl_context.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/init.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/init.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/input.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/input.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/monitor.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/monitor.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/osmesa_context.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/osmesa_context.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/vulkan.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/vulkan.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/wgl_context.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/wgl_context.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/win32_init.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/win32_init.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/win32_joystick.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/win32_joystick.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/win32_monitor.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/win32_monitor.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/win32_thread.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/win32_thread.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/win32_time.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/win32_time.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/win32_window.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/win32_window.c.obj"
  "M:/Classes/CSCI_441/lib/glfw/src/window.c" "M:/Classes/CSCI_441/program1/build/glfw/src/CMakeFiles/glfw.dir/window.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "UNICODE"
  "WINVER=0x0501"
  "_GLFW_USE_CONFIG_H"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "M:/Classes/CSCI_441/lib/glfw/include"
  "M:/Classes/CSCI_441/lib/glfw/src"
  "glfw/src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
