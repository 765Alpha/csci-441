#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out vec3 ourColor;

uniform mat4 modelMatrix;
uniform mat4 cameraMatrix;
uniform mat4 perspectiveMatrix;

void main()
{
	//mat4 transformationMatrix = cameraMatrix * modelMatrix;
	//vec4 transformedPos = transformationMatrix * vec4(aPos, 1);
	//gl_Position = vec4(transformedPos);
	//gl_Position = vec4(aPos, 1);
	
	vec4 newPos = perspectiveMatrix * cameraMatrix * modelMatrix * vec4(aPos, 1.0);
	gl_Position = vec4(newPos);
	
	ourColor = aColor;
}