#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>

float pi = 3.14159265359;

template <typename T, typename N, typename C>
void add_vertex(T& coords, const N& x, const N& y, const N& z,
		const C& r, const C& g, const C& b, bool with_noise=false) {
	// adding color noise makes it easier to see before shading is implemented
	float noise = 1-with_noise*(rand()%150)/100.;
	coords.push_back(x);
	coords.push_back(y);
	coords.push_back(z);
	coords.push_back(r*noise);
	coords.push_back(g*noise);
	coords.push_back(b*noise);
}

class Primitive {
public:
	std::vector<float> coords;
	GLfloat color[3] = {0.0f, 0.0f, 0.0f};
	int numberOfVertices;
	int verticesSize;
	Primitive() {}
};

class Square : public Primitive{
public:
	Square() {
		color[0] = 1.0f;
		color[1] = 0.0f;
		color[2] = 0.0f;
		numberOfVertices = 6;
		verticesSize = sizeof(float) * numberOfVertices * 6;
	
		add_vertex(coords, 0.5f, 0.5f, 0.0f, color[0], color[1], color[2]);
		add_vertex(coords, 0.5f, -0.5f, 0.0f, color[0], color[1], color[2]);
		add_vertex(coords, -0.5f, -0.5f, 0.0f,color[0], color[1], color[2]);
		add_vertex(coords, 0.5f, 0.5f, 0.0f, color[0], color[1], color[2]);
		add_vertex(coords, -0.5f, -0.5f, 0.0f, color[0], color[1], color[2]);
		add_vertex(coords, -0.5f, 0.5f, 0.0f, color[0], color[1], color[2]);
	}
};

class Triangle : public Primitive{
public:
	Triangle() {
		color[0] = 0.0f;
		color[1] = 1.0f;
		color[2] = 0.0f;
		numberOfVertices = 3;
		verticesSize = sizeof(float) * numberOfVertices * 6;
		
		add_vertex(coords, 0.5f, 0.5f, 0.0f, color[0], color[1], color[2]);
		add_vertex(coords, 0.5f, -0.5f, 0.0f, color[0], color[1], color[2]);
		add_vertex(coords, -0.5f, -0.5f, 0.0f, color[0], color[1], color[2]);
	}
};

class Circle : public Primitive{
public:
	Circle() {
		color[0] = 0.0f;
		color[1] = 0.0f;
		color[2] = 1.0f;
		
		int verticesInCircle = 36;
		numberOfVertices = 36 * 3;
		verticesSize = sizeof(float) * numberOfVertices * 6;
	
		float degreeStepSize = 360.0f / verticesInCircle;
		
		float centralVertexX = 0;
		float centralVertexY = 0;
		float radius = 0.5f;
		
		for(int i = 0; i < verticesInCircle; i++){
			float currentTheta = i * degreeStepSize;
			float currentX = radius * cos(currentTheta * pi / 180.0f);
			float currentY = radius * sin(currentTheta * pi / 180.0f);
			
			float nextTheta = (i+1) * degreeStepSize;
			float nextX = radius * cos(nextTheta * pi / 180.0f);
			float nextY = radius * sin(nextTheta * pi / 180.0f);
			
			add_vertex(coords, centralVertexX, centralVertexY, 0.0f, color[0], color[1], color[2]);
			add_vertex(coords, currentX, currentY, 0.0f, color[0], color[1], color[2]);
			add_vertex(coords, nextX, nextY, 0.0f, color[0], color[1], color[2]);
		}
	}
};

class Custom : public Primitive{
public:
	Custom(
		GLfloat* vertices,
		int verticesLength)
	{
		color[0] = 1.0f;
		color[1] = 1.0f;
		color[2] = 0.0f;
		
		numberOfVertices = verticesLength;
		verticesSize = numberOfVertices * sizeof(float);
	
		for(int i = 0; i < verticesLength; i++){
			float x = vertices[0 + (i * 6)];
			float y = vertices[1 + (i * 6)];
			float z = vertices[2 + (i * 6)];
			float r = vertices[3 + (i * 6)];
			float g = vertices[4 + (i * 6)];
			float b = vertices[5 + (i * 6)];
			
			add_vertex(coords, x, y, z, r, g, b);
		}
	}
};

#endif