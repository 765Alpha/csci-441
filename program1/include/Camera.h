#ifndef CAMERA_H
#define CAMERA_H

#include <Matrix.h>

class Camera
{
	public:
		Camera(
			GLfloat xCoord,
			GLfloat yCoord)
		{
			float coords[2] = {xCoord, yCoord};
			positionVector = &coords;
		}

		~Camera() {}

		void move(Matrix translationMatrix)
		{
			Matrix positionMatrix = Matrix(2, 1);
			positionMatrix.populate(positionVector);
			positionMatrix = &(translationMatrix * positionMatrix);
			float newPositionVector[] = {
				positionMatrix.getElementAt(0, 0),
				positionMatrix.getElementAt(0, 1)}
			positionVector = &newPositionVector;
		}

		GLfloat getX()
		{
			return positionVector.getElementAt(0, 0);
		}

		GLfloat getY()
		{
			return positionVector.getElementAt(0, 1);
		}

	private:
		float* positionVector;
};

#endif