#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <vector>

class FileInput{
public:
	int currentVerticeAmount;

	FileINput() {}

	float* processFileIn(std::ifstream& primitiveFile){
		std::vector<float> verticeArray;
		std::string delimiter = ",";
		
		std::string token;
		
		while(std::getline(primitiveFile, token)){
			std::cout << token << std::endl;
			
			int position = 0;
			
			position = token.find(delimiter);			
			std::string xString = token.substr(0, position);
			token.erase(0, position + delimiter.length());
			
			position = token.find(delimiter);	
			std::string yString = token.substr(0, position);
			token.erase(0, position + delimiter.length());
			
			position = token.find(delimiter);	
			std::string zString = token.substr(0, position);
			token.erase(0, position + delimiter.length());
			
			position = token.find(delimiter);	
			std::string rString = token.substr(0, position);
			token.erase(0, position + delimiter.length());
			
			position = token.find(delimiter);	
			std::string gString = token.substr(0, position);
			token.erase(0, position + delimiter.length());
			
			position = token.find(delimiter);	
			std::string bString = token.substr(0, position);
			token.erase(0, position + delimiter.length());
			
			add_vertex(
				verticeArray, 
				std::stof(xString),
				std::stof(yString),
				std::stof(zString),
				std::stof(rString),
				std::stof(gString),
				std::stof(bString));
		}	
	
		return &verticeArray[0];
	}
	
	int getNumberOfInVertices(std::ifstream& primitiveFile){
		std::string token;
		std::getline(primitiveFile, token);
		return std::stoi(token);
	}
	
	std::string queryForName(){
		std::string input;
		
		std::cout << "Please provide the name of the desired custom primitive: " << std::endl;
		std::cin >> input;
		
		return input;
	}
	
	float* startLoad(){
		//get name of primitive
		int gettingInput = 1;
		while(gettingInput == 1){
			std::string fileName = queryForName();
		
			std::ifstream primitiveFile;
			primitiveFile.open("../userGeneratedPrimitives/" + fileName + ".txt");

			if(primitiveFile.is_open()){
				currentVerticeAmount = getNumberOfInVertices(primitiveFile);				
				return processFileIn(primitiveFile);
			} else{
				std::cout << "Unable to open file." << std::endl;
			}
		}
	}
};