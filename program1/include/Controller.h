#ifndef CONTROLLER_H
#define CONTROLLER_H

class Controller
{
	public:
		//GLfloat xRotationDegree = 0.0f;
		//GLfloat yRotationDegree = 0.0f;
		GLfloat zRotationDegree = 0.0f;
		GLfloat modelTranslateX = 0.0f;
		GLfloat modelTranslateY = 0.0f;
		//GLfloat modelTranslateZ = 0.0f;
		GLfloat modelScale = 1.0f;
		GLfloat cameraLocationZ = -5.0f;
		GLfloat cameraLocationY = 0.0f;
		GLfloat cameraLocationX = 0.0f;
		//int primitiveIndex = 0; // 0 - cylinder   1 - cube
		//int projectionMode = 0;
		//bool primitiveChanged = false;
		bool loadPrimitive = false;
		bool clickHeldDown = false;
		double startingClickX;
		double startingClickY;
		double currentMouseX = 0.0f;
		double currentMouseY = 0.0f;
		float previousRotationZ = 0.0f;
		GLFWwindow* window;
		int screenWidth;
		int screenHeight;

		Controller() {}

		~Controller() {}

		void recieveUserInput()
		{
			if(clickHeldDown){
				handleClicks();
				return;
			} else {
				clickHeldDown = false;
			}
			int up = GetAsyncKeyState(VK_UP);
			int down = GetAsyncKeyState(VK_DOWN);
			int left = GetAsyncKeyState(VK_LEFT);
			int right = GetAsyncKeyState(VK_RIGHT);
			int comma = GetAsyncKeyState(VK_OEM_COMMA);
			//int out = GetAsyncKeyState(VK_OEM_PERIOD);
			//int xRotatePos = GetAsyncKeyState(0x55);
			//int xRotateNeg = GetAsyncKeyState(0x49);
			//int yRotatePos = GetAsyncKeyState(0x4F);
			//int yRotateNeg = GetAsyncKeyState(0x50);
			int zRotatePos = GetAsyncKeyState(VK_OEM_4);
			int zRotateNeg = GetAsyncKeyState(VK_OEM_6);
			int scaleUp = GetAsyncKeyState(VK_OEM_PLUS);
			int scaleDown = GetAsyncKeyState(VK_OEM_MINUS);
			int moveForward = GetAsyncKeyState(0x57);
			int moveBackward = GetAsyncKeyState(0x53);
			//int primitiveChange = GetAsyncKeyState(VK_SPACE);
			//int modeChange = GetAsyncKeyState(VK_OEM_5);

			//if (primitiveChange)
			//{
			//	primitiveIndex = (primitiveIndex + 1) % 3;
			//	std::cout << primitiveIndex << std::endl;
			//	primitiveChanged = true;
			//	Sleep(500);
			//}
			//if (modeChange)
			//{
			//	projectionMode = (projectionMode + 1) % 2;
			//	std::cout << "Mode changed to: " << projectionMode << std::endl;
			//	Sleep(500);
			//}
			if (right)
			{
				//cameraLocationX += 0.05f;
				modelTranslateX += 0.05f;
			}
			if (left)
			{
				//cameraLocationX -= 0.05f;
				modelTranslateX -= 0.05f;
			}
			if (up)
			{
				//cameraLocationY += 0.05f;
				modelTranslateY += 0.05f;
			}
			if (down)
			{
				//cameraLocationY -= 0.05f;
				modelTranslateY -= 0.05f;
			}
			//if (in)
			//{
			//	modelTranslateZ += 0.05f;
			//}
			//if (out)
			//{
			//	modelTranslateZ -= 0.05f;
			//}
			//if (xRotatePos)
			//{
			//	xRotationDegree += 1.0f;
			//}
			//if (xRotateNeg)		   
			//{					   
			//	xRotationDegree -= 1.0f;
			//}					   
			//if (yRotatePos)		   
			//{					   
			//	yRotationDegree += 1.0f;
			//}					   
			//if (yRotateNeg)		   
			//{					   
			//	yRotationDegree -= 1.0f;
			//}					   
			if (zRotatePos)
			{
				zRotationDegree += 1.0f;
			}
			if (zRotateNeg)		   
			{
				zRotationDegree -= 1.0f;
			}
			if (scaleUp)
			{
				modelScale += 0.02f;
			}
			if (scaleDown)
			{
				modelScale -= 0.02f;
			}
			if (moveForward && cameraLocationZ < 20.0f)
			{
				cameraLocationZ += 0.05f;
			}
			if (moveBackward && cameraLocationZ > -20.0f)
			{
				cameraLocationZ -= 0.05f;
			}
			if(comma)
			{
				loadPrimitive = true;
				Sleep(500);
			}
		}
		
		void handleClicks(){
			currentMouseX = startingClickX;
			currentMouseY = startingClickY;
			
			glfwGetCursorPos(window, &currentMouseX, &currentMouseY);
			
			float deltaTheta, deltaX, deltaY;
			deltaX = currentMouseX - startingClickX;
			deltaY = currentMouseY - startingClickY;
			if(deltaX == 0){
				return;
			}
			deltaTheta = atan(deltaY / deltaX) * 180 / 3.14159f;
			zRotationDegree = deltaTheta + previousRotationZ;
			
			float length;
			length = abs(deltaY / sin(deltaTheta * 3.14159f / 180)) * 0.01f;
			if(sin(deltaTheta * 3.14159f / 180) == 0){
				return;
			}
			modelScale = length;
		}
};

#endif