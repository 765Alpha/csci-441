#ifndef _CSCI441_CAMERA_H_
#define _CSCI441_CAMERA_H_

#include <csci441/matrix.h>
#include <csci441/vector.h>

class Camera {
public:
    Matrix projection;
	Matrix cameraMatrix;
    Vector eye;
    Vector origin;
    Vector up;
	Vector gaze;

    Camera() : eye(0,0,0), origin(0,0,0), up(0,0,0), gaze(0,0,0) {}

    Matrix look_at() const {
        Matrix mat;
        mat.look_at(eye, origin, up);
        return mat;
    }
	
    Matrix gazeAt() const {
        Matrix mat;
        mat.gazeAt(eye, gaze, up, cameraMatrix);
        return mat;
    }
};

#endif
