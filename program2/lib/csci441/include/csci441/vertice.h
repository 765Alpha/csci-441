#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>
#include <face>

class Vertex{
	public:
		Vector* coords;
		Vector* color;
		Face* faces;
		int facesSize = 0;
		
		Vertex(
			Vector inCoords,
			Vector inColor) :
			faces(new Face[128])
		{
			Vector temp = Vector(
				inCoords.x(),
				inCoords.y(),
				inCoords.z());
		
			coords = &temp;
				
			temp = Vector(
				inColor.x(),
				inColor.y(),
				inColor.z());
			
			color = &temp;
		}
		
		void addFace(Face inFace){
			faces[facesSize] = inFace;
			facesSize++;
		}
		
		Vector calculateSmoothNormal(){
			float aggregatedNormalX = 0;
			float aggregatedNormalY = 0;
			float aggregatedNormalz = 0;
			
			for(int i = 0; i < facesSize; i++){
				Vector faceNormal = faces[i].calculateFaceNormal();
				aggregatedNormalX += faceNormal.x();
				aggregatedNormalY += faceNormal.y();
				aggregatedNormalZ += faceNormal.z();
			}
			
			return Vector(
				aggregatedNormalX/facesSize,
				aggregatedNormalY/facesSize,
				aggregatedNormalZ/facesSize)
				.normalized();
		}
};

#endif