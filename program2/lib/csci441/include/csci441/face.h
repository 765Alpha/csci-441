#ifndef _CSCI441_FACE_H_
#define _CSCI441_FACE_H_

#include <cstdlib>
#include <vector>
#include "vertex.h"

class Face{
	public:
		Vertex* v1;
		Vertex* v2;
		Vertex* v3;
		int v1Index, v2Index, v3Index;
		
		Face(
			Vertex inVUno,
			Vertex inVDos,
			Vertex inVTres)
		{
			Vertex temp = inVUno;
			v1 = &temp;
			
			temp = inVDos;
			v2 = &temp;
			
			temp = inVTres;
			v3 = &temp;
		}
		
		Vector calculateFaceNormal(){
			x1 = v1.x();
			x2 = v2.x();
			x3 = v3.x();
			y1 = v1.y();
			y2 = v2.y();
			y3 = v3.y();
			z1 = v1.z();
			z2 = v2.z();
			z3 = v3.z();
			x1Vec = x3 - x1;
			x2Vec = x2 - x1;
			y1Vec = y3 - y1;
			y2Vec = y2 - y1;
			z1Vec = z3 - z1;
			z2Vec = z2 - z1;
			xNorm = y1Vec * z2Vec - z1Vec * y2Vec;
			yNorm = z1Vec * x2Vec - x1Vec * z2Vec;
			zNorm = x1Vec * y2Vec - y1Vec * x2Vec;
		
			Vector normal = Vector(
				xNorm,
				yNorm,
				zNorm);
				
			return normal.normalize();
		}
};

#endif