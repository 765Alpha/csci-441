	This program loads in a maze obj file called "maze" in the models folder (values for v should be between -1 and 1)
and scales it to fill the screen in the default birds-eye view. It will also load a character obj called "character"
from the same folder. This is scaled down and placed at the top right corner, where it's the "standard" to start.
Because of this scaling, as long as your pathways are .2 units across, any character/maze combination loaded in will
fit together nicely (as long as the coords are between -1 and 1. Looking at you ducky).

	In birds-eye view, using the arrow keys will move the character in the direction pressed (up will go North, etc).
By pressing space, you will transfer to first-person view. The arrow keys will now be move forward, backwards, and 
side-to-side respectively. There is no pitch or yaw control. You can use A and D to turn 90 degrees left or right,
but there is currently a bug where the character and camera are rotated around the z-axis and de-sync, so this is
pretty much useless. Good luck in first-person, as you will only be facing one direction. You will notice that when
switching your view, the character and first-person camera will be persistent and move the same.

	By default faceted shading is turned on. To change this, press the '\' key. This might look weird depending on your
maze. If when creating the maze you created longer walls with only two triangles like I did, then the vertices that those
faces "passes" don't have normal information for them. This means that corners intercepting these walls look off.
To fix this, one would create a square for each step in their "grid" with vertices and faces (for instance, I would create
10 intermediate vertices on the South wall and matching faces instead of only using x = -1 and x = 1).

	Some fun facts about the structure of the program has to deal with how we (from lab6) dealt with a "more elegant"
storage structure instead of polygon soup. We doubly linked (ish) our verticies and faces by having the vertices keep track
of which faces are using them as they're created, while the faces keep track of the vertices that it contains. This means
that when it comes time to calculate smooth normals, each vertex only needs to calculate the faceted normal of its
attached faces and average them. This also means that for each line of an obj file (from the line prefixes given by the duck)
also has an object that it will create, keeping the flow of the program much simpler.

	The ObjLoader class is (for our purposes) completely dynamic and doesn't care about what it is loading. Since it is
simply a parser, it will create a basic Primitive with all the vertices and faces connected and coords calculated. 
If you want to change the color of an object as it is being read from the .obj file, when creating said object call
setColor() on the loader and presto, you've got it.

	I also hand typed out my maze and character obj files. Hand made, organic primitives instead of cheaply made,
auto generated, GMO primitives. You can really taste the salt from having to keep track of over 100 vertices manually.
Quite delicious.