#ifndef _CSCI441_OBJLOADER_H_
#define _CSCI441_OBJLOADER_H_

#include <cstdlib>
#include <vector>
#include "shape.h"
#include "face.h"
#include "vertex.h"

class ObjLoader{
public:
	Primitive object;
	float r;
	float g;
	float b;

	ObjLoader() {
		object = Primitive();
	}
	
	void setColor(
		float inR,
		float inG,
		float inB)
	{
		r = inR;
		g = inG;
		b = inB;
	}
	
	void load(std::string fileName){
		std::ifstream objectFile;
		objectFile.open("../models/" + fileName + ".obj");
		
		if(!objectFile.is_open()){
			std::cout << "File " << fileName << " could not be opened." << std::endl;
			return;
		}
		
		//Vertex* vertices;
		std::vector<Vertex> vertices;
		int verticesSize = 0;
		//Face* faces;
		std::vector<Face> faces;
		int facesSize = 0;
		
		std::string delimiter = " ";
		std::string token;
			
		float maxCoord = 0;
		
		while(std::getline(objectFile, token)){
			if(token[0] == '#'
				|| token.empty())
			{
				continue;
			}
			
			if(token[0] == 'v'){
				int position = token.find(delimiter);
				std::string v = token.substr(0, position);
				token.erase(0, position + delimiter.length());
				
				position = token.find(delimiter);	
				std::string xString = token.substr(0, position);
				token.erase(0, position + delimiter.length());
				
				position = token.find(delimiter);	
				std::string yString = token.substr(0, position);
				token.erase(0, position + delimiter.length());
				
				position = token.find(delimiter);	
				std::string zString = token.substr(0, position);
				token.erase(0, position + delimiter.length());
				
				if(maxCoord < abs(std::stof(xString))){
					maxCoord = abs(std::stof(xString));
				}
				else if (maxCoord < abs(std::stof(yString))){
					maxCoord = abs(std::stof(yString));
				}
				else if (maxCoord < abs(std::stof(zString))){
					maxCoord = abs(std::stof(zString));
				}
				
				Vector coords = Vector(
					std::stof(xString),
					std::stof(yString),
					std::stof(zString));
					
				Vector color = Vector(r,g,b);
				
				Vertex toBeVertex = Vertex(
					&coords,
					&color);
					
				vertices.push_back(toBeVertex);
				
				verticesSize++;
			}
			
			if(token[0] == 'f'){
				int position = token.find(delimiter);
				std::string f = token.substr(0, position);
				token.erase(0, position + delimiter.length());
				
				position = token.find(delimiter);
				std::string v1String = token.substr(0, position);
				token.erase(0, position + delimiter.length());
				
				position = token.find(delimiter);	
				std::string v2String = token.substr(0, position);
				token.erase(0, position + delimiter.length());
				
				position = token.find(delimiter);	
				std::string v3String = token.substr(0, position);
				token.erase(0, position + delimiter.length());
				
				Vertex v1 = vertices[std::stoi(v1String)];
				Vertex v2 = vertices[std::stoi(v2String)];
				Vertex v3 = vertices[std::stoi(v3String)];
				
				Face newFace = Face();
					
				newFace.v1Index = std::stoi(v3String) - 1;
				newFace.v2Index = std::stoi(v2String) - 1;
				newFace.v3Index = std::stoi(v1String) - 1;
	
				faces.push_back(newFace);
					
				facesSize++;
			}
		}
		
		for(int i = 0; i < faces.size(); i++){
			Face* currentFace = &faces[i];
			
			currentFace->v1 = &vertices[currentFace->v1Index];
			currentFace->v2 = &vertices[currentFace->v2Index];
			currentFace->v3 = &vertices[currentFace->v3Index];
			
			currentFace->v1->addFace(*currentFace);
			currentFace->v2->addFace(*currentFace);
			currentFace->v3->addFace(*currentFace);
		}
		
		for(int i = 0; i < faces.size(); i++) {
			Face currentFace = faces[i];
			
			float normal1Args[9] = {
				currentFace.v1->getX(),
				currentFace.v1->getY(),
				currentFace.v1->getZ(),
				currentFace.v2->getX(),
				currentFace.v2->getY(),
				currentFace.v2->getZ(),
				currentFace.v3->getX(),
				currentFace.v3->getY(),
				currentFace.v3->getZ()};
			Vector v1FascettedNormal = object.calculateFastedNormal(normal1Args);

			Vector v1SmoothNormal = currentFace.v1->calculateSmoothNormal();
			
			add_vertex(
				object.coords,
				currentFace.v1->getX(),
				currentFace.v1->getY(),
				currentFace.v1->getZ(),
				r,
				g,
				b,
				v1FascettedNormal,
				v1SmoothNormal);
			
			Vector v2SmoothNormal = currentFace.v2->calculateSmoothNormal();
			
			add_vertex(
				object.coords,
				currentFace.v2->getX(),
				currentFace.v2->getY(),
				currentFace.v2->getZ(),
				r,
				g,
				b,
				v1FascettedNormal,
				v2SmoothNormal);

			Vector v3SmoothNormal = currentFace.v3->calculateSmoothNormal();
			
			add_vertex(
				object.coords,
				currentFace.v3->getX(),
				currentFace.v3->getY(),
				currentFace.v3->getZ(),
				r,
				g,
				b,
				v1FascettedNormal,
				v3SmoothNormal);
		}
	}
};

#endif