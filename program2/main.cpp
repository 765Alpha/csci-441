#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"
#include "face.h"
#include "vertex.h"
#include "objLoader.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
	return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
	return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(
	const Matrix& model,
	GLFWwindow *window,
	int directionCorrection,
	int controlSceme)
{
	Matrix trans;

	const float ROT = 1;
	const float SCALE = .05;
	const float TRANS = .01;

	if(controlSceme == 0) {
		// ROTATE
		//if (isPressed(window, GLFW_KEY_U)) { trans.rotate_x(-ROT); }
		//else if (isPressed(window, GLFW_KEY_I)) { trans.rotate_x(ROT); }
		//else if (isPressed(window, GLFW_KEY_O)) { trans.rotate_y(-ROT); }
		//else if (isPressed(window, GLFW_KEY_P)) { trans.rotate_y(ROT); }
		//else if (isPressed(window, '[')) { trans.rotate_z(-ROT); }
		//else if (isPressed(window, ']')) { trans.rotate_z(ROT); }
		//// SCALE
		//else if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
		//else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
		// TRANSLATE
		if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0, TRANS*directionCorrection, 0); }
		else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0, -TRANS*directionCorrection, 0); }
		else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(-TRANS*directionCorrection, 0, 0); }
		else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(TRANS*directionCorrection, 0, 0); }
		//else if (isPressed(window, ',')) { trans.translate(0,0,TRANS); }
		//else if (isPressed(window, '.')) { trans.translate(0,0,-TRANS); }
	}
	else if(controlSceme == 1) {
		if (isPressed(window, GLFW_KEY_UP)) { trans.translate(-TRANS*directionCorrection, 0, 0); }
		else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(TRANS*directionCorrection, 0, 0); }
		else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(0, -TRANS*directionCorrection, 0); }
		else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(0, TRANS*directionCorrection, 0); }
		else if (isPressed(window, GLFW_KEY_A)) {
			trans.rotate_z(90);
			Sleep(250);
		}
		else if (isPressed(window, GLFW_KEY_D)) {
			trans.rotate_z(-90);
			Sleep(250);
		}
	}

	return trans * model;
}

void processInput(Matrix& model, Matrix& camera, int controlSceme, GLFWwindow *window) {
	if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
		glfwSetWindowShouldClose(window, true);
	}
	model = processModel(model, window, 1, controlSceme);
	camera = processModel(camera, window, -1, controlSceme);
}

void errorCallback(int error, const char* description) {
	fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
	GLFWwindow* window;

	glfwSetErrorCallback(errorCallback);

	/* Initialize the library */
	if (!glfwInit()) { return -1; }

	glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	// tell glfw what to do on resize
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	// init glad
	if (!gladLoadGL()) {
		std::cerr << "Failed to initialize OpenGL context" << std::endl;
		glfwTerminate();
		return -1;
	}

	// create obj
	//Model obj(
	//	Torus(40, .75, .5, 1, .2, .4).coords,
	//	Shader("../vert.glsl", "../frag.glsl"));

	// make a floor
	Model floor(
		DiscoCube().coords,
		Shader("../vert.glsl", "../frag.glsl"));
		
	ObjLoader mazeLoader = ObjLoader();
	mazeLoader.setColor(1, 0, 0);
	mazeLoader.load("maze");
	Model maze(
		mazeLoader.object.coords,
		Shader("../vert.glsl", "../frag.glsl"));
	Matrix mazeScale;
	mazeScale.scale(2.0f, 2.0f, 2.0f);
	maze.model = mazeScale;
	
	ObjLoader characterLoader = ObjLoader();
	characterLoader.setColor(0, 1, 0);
	characterLoader.load("character");
	Model character(
		characterLoader.object.coords,
		Shader("../vert.glsl", "../frag.glsl"));
	Matrix characterScale, characterTrans;
	characterScale.scale(0.1f, 0.1f, 0.1f);
	characterTrans.translate(2, 2, 0);
	character.model = characterTrans * characterScale;
	
	
	Matrix floor_trans, floor_scale;
	floor_trans.translate(0, 0, -1);
	floor_scale.scale(100, 100, 1);
	floor.model = floor_trans*floor_scale;
	floor.smoothShading = 0;

	// setup camera
	Matrix projection;
	projection.perspective(45, 1, .01, 10);

	Camera birdCamera;
	birdCamera.projection = projection;
	birdCamera.eye = Vector(0, 0, 2);
	birdCamera.gaze = Vector(0, 0, -1);
	birdCamera.up = Vector(0, 1, 0);
	
	Camera firstPersonCamera;
	firstPersonCamera.projection = projection;
	firstPersonCamera.eye = Vector(1.9, 2, 0);
	firstPersonCamera.gaze = Vector(-1, 0, 0);
	firstPersonCamera.up = Vector(0, 0, 1);

	// and use z-buffering
	glEnable(GL_DEPTH_TEST);

	// create a renderer
	Renderer renderer;

	// set the light position
	Vector lightPos(2.0f, 2.0f, 5.0f);
	
	//0 is bird, 1 is first person
	int cameraType = 0;

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window)) {
		// process input
		processInput(
			character.model,
			firstPersonCamera.cameraMatrix,
			cameraType,
			window);

		if(isPressed(window, ' ')){
			//maze.smoothShading = abs(maze.smoothShading - 1);
			//character.smoothShading = abs(character.smoothShading - 1);
			cameraType = abs(cameraType - 1);
			Sleep(250);
		}
		
		if(isPressed(window, '\\')){
			maze.smoothShading = abs(maze.smoothShading - 1);
			character.smoothShading = abs(character.smoothShading - 1);
			Sleep(250);
		}
		
		Camera* camera;
		
		if(cameraType == 0){
			camera = &birdCamera;
		}
		else if(cameraType == 1){
			camera = &firstPersonCamera;
		}
		
		/* Render here */
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		// render the object and the floor
		renderer.render(*camera, maze, lightPos);
		renderer.render(*camera, character, lightPos);
		renderer.render(*camera, floor, lightPos);

		/* Swap front and back and poll for io events */
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}
