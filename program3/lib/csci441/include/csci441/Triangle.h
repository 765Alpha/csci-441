#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "vector.h"

class Triangle
{
	public:
		Vector corner1 = Vector(0,0,0);
		Vector corner2 = Vector(0,0,0);
		Vector corner3 = Vector(0,0,0);
		float red;
		float green;
		float blue;
		bool isReflective = true;
	
	public:
		Triangle(
			float corner1X,
			float corner1Y,
			float corner1Z,
			float corner2X,
			float corner2Y,
			float corner2Z,
			float corner3X,
			float corner3Y,
			float corner3Z,
			float inRed,
			float inGreen,
			float inBlue)
		{
			corner1 = Vector(
				corner1X,
				corner1Y,
				corner1Z);
				
			corner2 = Vector(
				corner2X,
				corner2Y,
				corner2Z);
				
			corner3 = Vector(
				corner3X,
				corner3Y,
				corner3Z);
				
			red = inRed;
			blue = inBlue;
			green = inGreen;
		}
			
		Triangle() {}
			
		Vector getCorner1(){
			return corner1;
		}
		
		Vector getCorner2(){
			return corner2;
		}
		
		Vector getCorner3(){
			return corner3;
		}
		
		int getRed()
		{
			return red;
		}
		
		int getGreen()
		{
			return green;
		}
		
		int getBlue()
		{
			return blue;
		}
		
		void setReflective(bool reflective){
			isReflective = reflective;
		}
};

#endif