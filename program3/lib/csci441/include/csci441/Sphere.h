#ifndef SPHERE_H
#define SPHERE_H

class Sphere
{
	float centerX;
	float centerY;
	float centerZ;
	float radius;
	float red;
	float green;
	float blue;
	
	public:
		Sphere(
			float inCenterX,
			float inCenterY,
			float inCenterZ,
			float inRadius,
			float inRed,
			float inGreen,
			float inBlue) :
			centerX(inCenterX),
			centerY(inCenterY),
			centerZ(inCenterZ),
			radius(inRadius),
			red(inRed),
			green(inGreen),
			blue(inBlue) {}
			
		Sphere() {}
			
		float getCenterX()
		{
			return centerX;
		}
			
		float getCenterY()
		{
			return centerY;
		}
			
		float getCenterZ()
		{
			return centerZ;
		}
			
		float getRadius()
		{
			return radius;
		}
		
		int getRed()
		{
			return red;
		}
		
		int getGreen()
		{
			return green;
		}
		
		int getBlue()
		{
			return blue;
		}
};

#endif