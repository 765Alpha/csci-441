#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>
#include <csci441/Ray.h>
#include <csci441/Sphere.h>
#include <csci441/Triangle.h>

#include "bitmap_image.hpp"


const int WIDTH = 500;
const int HEIGHT = 500;
const bool orthogonal = false;

Vector getColorOfRay(
	Ray ray,
	Sphere* spheres,
	int numberOfSpheres);
	
Vector getTriangleColor(
	Ray ray,
	Triangle* triangles,
	int numberOfTriangles,
	int depth,
	int reflectedIndex);

int main(void)
{
	//Sphere * spheres;
	//spheres = new Sphere[3];
	//spheres[0] =
	//	*(new Sphere(
	//		1.0f,
	//		0.0f,
	//		0.0f,
	//		0.3f,
	//		255,
	//		1,
	//		1));
	//spheres[1] =
	//	*(new Sphere(
	//		1.0f,
	//		0.6f,
	//		0.6f,
	//		0.5f,
	//		2,
	//		176,
	//		54));
	//spheres[2] =
	//	*(new Sphere(
	//		1.0f,
	//		0.0f,
	//		-0.7f,
	//		0.2f,
	//		150,
	//		3,
	//		200));
	int numberOfTriangles = 4;
	Triangle* triangles = new Triangle[numberOfTriangles];
	triangles[0] = Triangle(
		0.5f, 0.5f, -0.6f,
		0.5f, -0.5f, -0.6f,
		-0.5f, 0.5f, -0.3f,
		0, 255, 0);
	triangles[1] = Triangle(
		0.5f, -0.5f, -0.6f,
		-0.5f, -0.5f, -0.3f,
		-0.5f, 0.5f, -0.3f,
		0, 255, 0);
	triangles[2] = Triangle(
		0.5f,  -2.0f,  -1.0f,
		1.0f, -2.0f,  -0.0f,
		1.0f, 2.0f, -0.0f,
		255, 0, 0);
	triangles[3] = Triangle(
		0.5f, -2.0f, -1.0f,
		1.0f, 2.0f, -0.0f,
		0.5f, 2.0f, -1.0f,
		255, 0, 0);
		
	triangles[2].setReflective(false);
	triangles[3].setReflective(false);
	
	std::vector<Ray> rays;
	rays.reserve(WIDTH * HEIGHT);
	float left = -1.0f;
	float right = 1.0f;
	float top = 1.0f;
	float bottom = -1.0f;
	
	if (orthogonal)
	{
		for (int i = 0; i < WIDTH; i++)
		{
			for (int j = 0; j < HEIGHT; j++)
			{
				float originY = left + (right - left) * ((float)i / (float)WIDTH);
				float originZ = top + (bottom - top) * ((float)j / (float)HEIGHT);
				rays.push_back(Ray(
					0.0f,
					originY,
					originZ,
					0.5f,
					0.0f,
					0.0f));
			}
		}
	}
	else
	{
		for (int i = 0; i < WIDTH; i++)
		{
			for (int j = 0; j < HEIGHT; j++)
			{
				float viewportY = left + (right - left) * ((float)i / (float)WIDTH);
				float viewportZ = top + (bottom - top) * ((float)j / (float)HEIGHT);
				float viewportDistance = 0.5f;
				rays.push_back(Ray(
					0.0f,
					0.0f,
					0.0f,
					viewportY,
					viewportZ,
					viewportDistance));
				
				//rays.push_back(Ray(
				//	0.0f,
				//	0.0f,
				//	0.0f,
				//	viewportDistance,
				//	viewportY,
				//	viewportZ));
			}
		}
	}
		
    bitmap_image image(WIDTH, HEIGHT);
	
	for (int i = 0; i < WIDTH; i++)
	{
		for (int j = 0; j < HEIGHT; j++)
		{
			//Vector colorVector = getColorOfRay(
			//	rays.at(WIDTH * i + j),
			//	spheres,
			//	3);
			
			Vector colorVector = getTriangleColor(
				rays.at(WIDTH * i + j),
				triangles,
				numberOfTriangles,
				0,
				-1);
				
			int red = colorVector.x();
			int green = colorVector.y();
			int blue = colorVector.z();
			
			if (red == -1)
			{
				red = 0;
			}
			if (green == -1)
			{
				green = 0;
			}
			if (blue == -1)
			{
				blue = 0;
			}
			
			image.set_pixel(
				i,
				j,
				red,
				green,
				blue);
		}
	}
	
    image.save_image("rayTracedImage.bmp");
    std::cout << "Finished" << std::endl;
	
	return 0;
}

Vector getTriangleColor(
	Ray ray,
	Triangle* triangles,
	int numberOfTriangles,
	int depth,
	int reflectedIndex)
{
	Vector direction = Vector(
		ray.getDirectionX(),
		ray.getDirectionY(),
		ray.getDirectionZ()).normalized();
	
	Vector eye = Vector(
		ray.getOriginX(),
		ray.getOriginY(),
		ray.getOriginZ());
		
	int red = -1;
	int green = -1;
	int blue = -1;
	float lowestT = 999999.9f;
	
	for (int i = 0; i < numberOfTriangles; i++)
	{
		if(i == reflectedIndex){
			continue;
		}
	
		Triangle triangle = triangles[i];
		Vector p1 = triangle.corner1;
		Vector p2 = triangle.corner2;
		Vector p3 = triangle.corner3;
	
		//https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/ray-triangle-intersection-geometric-solution
		//find Normal of triangles
		Vector ba = p2 - p1;
		Vector bc = p2 - p3;
		Vector normal = ba.cross(bc).normalized();
		
		//find D of plane equation
		float d = normal * p1;
	
		//find p
		float t = -((normal * eye) + d) / (normal * direction);
			
		if(depth != 0){
			//std::cout << t << std::endl;
			t *= -1;
		}
		
		Vector position = eye + (direction.scale(t));
		
		//check barycentric
		float lambda1 = ((p2.y() - p3.y()) * (position.x() - p3.x()) + (p3.x() - p2.x()) * (position.y() - p3.y())) /
			((p2.y() - p3.y()) * (p1.x() - p3.x()) + (p3.x() - p2.x()) * (p1.y() - p3.y()));
		float lambda2 = ((p3.y() - p1.y()) * (position.x() - p3.x()) + (p1.x() - p3.x()) * (position.y() - p3.y())) /
			((p2.y() - p3.y()) * (p1.x() - p3.x()) + (p3.x() - p2.x()) * (p1.y() - p3.y()));
		float lambda3 = 1 - lambda1 - lambda2;
		
		if(lambda1 >= 0 && lambda1 <= 1
			&& lambda2 >= 0 && lambda2 <= 1
			&& lambda3 >= 0 && lambda3 <= 1
			&& t > 0)
		{		
			red = triangle.getRed();
			blue = triangle.getBlue();
			green = triangle.getGreen();
			
			//https://www.scratchapixel.com/lessons/3d-basic-rendering/introduction-to-shading/reflection-refraction-fresnel
			//if reflection, create new ray at p with reflected direction
			if(triangle.isReflective){
				Vector reflectedVector =
					direction - normal.scale(direction * normal).scale(2);
				//reflectedVector = reflectedVector.normalized();
			
				Ray reflectedRay = Ray(
					position.x(),
					position.y(),
					position.z(),
					reflectedVector.x(),
					reflectedVector.y(),
					reflectedVector.z());
				
				Vector reflectionColor = getTriangleColor(
					reflectedRay,
					triangles,
					numberOfTriangles,
					depth + 1,
					i);
					
				int refRed = reflectionColor.x();
				int refGreen = reflectionColor.y();
				int refBlue = reflectionColor.z();
					
				if (refRed == -1)
				{
					refRed = 0;
				}
				if (refGreen == -1)
				{
					refGreen = 0;
				}
				if (refBlue == -1)
				{
					refBlue = 0;
				}
					
				float surfaceStrength = 0.2;
					
				red = (surfaceStrength * red) + (refRed * (1-surfaceStrength));
				green = (surfaceStrength * green) + (refGreen * (1-surfaceStrength));
				blue = (surfaceStrength * blue) + (refBlue * (1-surfaceStrength));
			}
		
			if(lowestT > t){
				lowestT = t;
			} else {
				continue;
			}
		}
	}
		
	return Vector(
		red,
		green,
		blue);
}

Vector getColorOfRay(
	Ray ray,
	Sphere* spheres,
	int numberOfSpheres)
{
	Vector direction = Vector(
		ray.getDirectionX(),
		ray.getDirectionY(),
		ray.getDirectionZ()).normalized();
	
	Vector eye = Vector(
		ray.getOriginX(),
		ray.getOriginY(),
		ray.getOriginZ());
	
	int red = -1;
	int green = -1;
	int blue = -1;
	float lowestT = 999999.9f;
	for (int i = 0; i < numberOfSpheres; i++)
	{
		float radius = spheres[i].getRadius();
		
		Vector center = Vector(
			spheres[i].getCenterX(),
			spheres[i].getCenterY(),
			spheres[i].getCenterZ());
			
		float a = direction * direction;
		float b = direction.scale(2) * (eye - center);
		float c = (eye - center) * (eye - center) - radius * radius;
		
		float determinant = b * b - 4 * a * c;
		
		float bigT, lilT;
		bigT = (-b + pow(b * b - 4 * a * c, 0.5) ) / (2 * a);
		lilT = (-b - pow(b * b - 4 * a * c, 0.5) ) / (2 * a);
		
		if (bigT < lilT &&
			bigT >= 0 &&
			bigT < lowestT)
		{
			lowestT = bigT;
			red = spheres[i].getRed();
			green = spheres[i].getGreen();
			blue = spheres[i].getBlue();
		}
		if (lilT < bigT &&
			lilT >= 0 &&
			lilT < lowestT)
		{
			lowestT = lilT;
			red = spheres[i].getRed();
			green = spheres[i].getGreen();
			blue = spheres[i].getBlue();
		}
	}
		
	return Vector(
		red,
		green,
		blue);
}



























