What the program does:
	This program implements ray tracing of triangles and reflection.
	It first finds the normal of the triangle's plane, calculates the t value
	where the ray intersects this plane, then calculates the Barycentric
	coordinates of the intersection with regards to the triangle's points.
	If within the triangle, that color is used unless a t value is calculated
	that is less than that triangle. Then reflection is calculated from that
	point, which will be talked about in the +1 PDF.
	
	To use, simply run program3.exe. If you would like to customize (not
	recommended, as somewhere along the way the coordinate system was
	altered and hasn't been quite fixed yet, so hardcoding triangles feels
	terrible and comes to trial and error to place correctly), in main.cpp
	there's a list of hardcoded triangles. If you want a triangle to become
	reflective, then use setReflective(bool) as shown on line 93. The
	default is true. If adding/removing triangles, make sure to change the amount
	on line 70. If you want to have surfaces keep more/less of their color when
	reflecting, increase/decrease the value on line 306. To set background color,
	change the values of lines 295, 299, and 303.
	
	The scene is a green mirror on the right hand side and a red square
	(non-reflective) in the middle.
	
Other Information:
	There is a bug where it seems reflected rays are being pointed towards
	the center of the mirror, as the reflection is bigger than the object itself
	(maybe, as it's hard to tell exactly how big the red square is due to the
	messed up coordinate system).