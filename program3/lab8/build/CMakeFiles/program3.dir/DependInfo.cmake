# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "M:/Classes/CSCI_441/csci-441/program3/lib/glad/src/glad.c" "M:/Classes/CSCI_441/csci-441/program3/lab8/build/CMakeFiles/program3.dir/M_/Classes/CSCI_441/csci-441/program3/lib/glad/src/glad.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../../lib/glfw/include"
  "../../lib/glad/include"
  "../../lib/csci441/include"
  "../../lib/SFML/include"
  "M:/Classes/CSCI_441/csci-441/program3/lib/glfw/include"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "M:/Classes/CSCI_441/csci-441/program3/lab8/main.cpp" "M:/Classes/CSCI_441/csci-441/program3/lab8/build/CMakeFiles/program3.dir/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../lib/glfw/include"
  "../../lib/glad/include"
  "../../lib/csci441/include"
  "../../lib/SFML/include"
  "M:/Classes/CSCI_441/csci-441/program3/lib/glfw/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "M:/Classes/CSCI_441/csci-441/program3/lab8/build/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
